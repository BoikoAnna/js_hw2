//Теоретичні питання

//Які існують типи даних у Javascript?
//number (для чисел); bigint (для цілих чисел довжини до ±2^53);
//string для рядків (може містити декілька символів);'Boolean' ('true/false');
//null (для невідомих значень null); undefined (для неприсвоєних значень);
//object(для декілької данних - структур);symbol (для унікальних ідентифікаторів);


//У чому різниця між == і ===?
//== - це оператор порівняння, при порівнянні рядка із числом JavaScript перетворює
//будь-який рядок у число, порожній рядок на нуль, рядок без числа у NaN-false.
//=== - це строгий оператор порівняння,повертає false для значень, що не мають подібний тип.
// Якщо ми порівняємо 1 із "1"- поверне помилкове значення.

//Що таке оператор?
// елемент JavaScript, за допомогою якого ми описуємо дію для виконання


//Завдання
//Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

//Технічні вимоги:

//Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
//Якщо вік менше 18 років - показати на екрані повідомлення: 
//You are not allowed to visit this website.

//Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням:
// Are you sure you want to continue? і кнопками Ok, Cancel. 
//Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome,  + ім'я користувача.
// Якщо користувач натиснув Cancel, показати на екрані повідомлення:
// You are not allowed to visit this website.

//Якщо вік більше 22 років – показати на екрані повідомлення: Welcome,  + ім'я користувача.

//Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
//var була доступна в JavaScript з початкової версії, а let і const стали доступними тільки в ES6 (ES2015) і вище.

//Після введення даних додати перевірку їхньої коректності. 
//Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново
// (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).


let name = "";
let age = "";

do {
name = prompt("Як Вас звати?");
}

while(name === "");

do {
age = +prompt('Скільки вам років?');
}
while(isNaN(age) || age === 0);

console.log(name, age);

if (age<=18) {
alert('You are not allowed to visit this website');
}

if (age>=18 && age<=22) { 
let confirmVisit = confirm(`Are you sure you want to continue?`);
if (confirmVisit) {
alert( `Welcome, ${name}!` );}
else {
alert ('You are not allowed to visit this website');} 
}

if(age>22) {
alert( `Welcome, ${name}!`);}